//
// Created by ovidiu on 13/10/15.
//

#include <iostream>
#include "ComponentApp.h"
#include "../helper/Helper.h"
#include "../message/SimpleMessage.h"

using namespace std;

void ComponentApp::messageReceived(Message &message) {
    logger::info("ComponentApp", "Message received: " + message.toString());
}
