//
// Created by ovidiu on 13/10/15.
//

#include <iostream>
#include <rude/config.h>
#include "ComponentApp.h"
#include "../protocol/client/GenericClient.h"
#include "../protocol/client/ZeroMqClient.h"
#include "../message/SimpleMessage.h"
#include "../helper/Helper.h"

#define APP_NAME "ZeroMQ App"
#define PROPERTY_MODULE_ZMQ_ID "module.zmq.id"
#define PROPERTY_MODULE_ZMQ_PROTOCOL "module.zmq.protocol"
#define PROPERTY_MODULE_ZMQ_IP "module.zmq.remote.ip"
#define PROPERTY_MODULE_ZMQ_SUBSCRIBE "module.zmq.subscribe.port"
#define PROPERTY_MODULE_ZMQ_PUBLISH "module.zmq.publish.port"

using namespace std;
using namespace rude;

/*
 * The ZeroMQ App, which uses a direct connection to the CoreApplication
 */

GenericClient *initClient(Config &config, ComponentApp *manager) {
    GenericClient *client = new ZeroMqClient((unsigned long) config.getIntValue(PROPERTY_MODULE_ZMQ_ID),
                                             config.getStringValue(PROPERTY_MODULE_ZMQ_PROTOCOL),
                                             config.getStringValue(PROPERTY_MODULE_ZMQ_IP),
                                             config.getIntValue(PROPERTY_MODULE_ZMQ_SUBSCRIBE),
                                             config.getIntValue(PROPERTY_MODULE_ZMQ_PUBLISH));

    client->start(manager);
    return client;
}

int main(int args, char **argv) {
    if (args != 3) {
        logger::error(APP_NAME, "Usage: " + string(argv[0]) + " <project conf file> <conf profile>");
        return 1;
    }

    Config config;
    if (config.load(argv[1])) {
        if (config.setSection(argv[2], false)) {
            // create a generic component application that prints every message received
            ComponentApp *componentApp = new ComponentApp();
            GenericClient *client = initClient(config, componentApp);

            std::this_thread::sleep_for(std::chrono::seconds(1));

            string line = " ";
            while (!line.empty()) {
                logger::info(APP_NAME, "Enter your message (an empty line will end the program): ");
                getline(cin, line);
                if (!line.empty()) {
                    // the message is a concatenation of a random int + the line read from stdin
                    Message *message = new SimpleMessage(rand(), line);
                    client->publish(*message);
                    delete message;
                }
            }

            client->close();
            delete client;
            delete componentApp;
            return 0;
        } else {
            logger::error(APP_NAME, "Could not load configuration profile: " + string(argv[2]));
            return 1;
        }
    } else {
        logger::error(APP_NAME, "Could not load configuration file: " + string(argv[1]));
        return 1;
    }
}