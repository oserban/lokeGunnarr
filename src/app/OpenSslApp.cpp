//
// Created by ovidiu on 15/10/15.
//

#include <chrono>
#include <thread>
#include <iostream>
#include <rude/config.h>
#include "../message/SimpleMessage.h"
#include "../protocol/client/GenericClient.h"
#include "ComponentApp.h"
#include "../protocol/client/OpenSslClient.h"
#include "../helper/Helper.h"

#define APP_NAME "OpenSSL App"
#define PROPERTY_MODULE_OPENSSL_ID "module.openssl.id"
#define PROPERTY_MODULE_OPENSSL_LOCAL_PORT "module.openssl.localPort"
#define PROPERTY_MODULE_OPENSSL_CERTIFICATE "module.openssl.certificate"
#define PROPERTY_MODULE_OPENSSL_KEY "module.openssl.key"
#define PROPERTY_MODULE_OPENSSL_REMOTE_IP "module.openssl.remote.ip"
#define PROPERTY_MODULE_OPENSSL_REMOTE_PORT "module.openssl.remote.port"

using namespace std;
using namespace rude;

/*
 * The OpenSSL App, which connects directly to the CoreApplication
 */

GenericClient *initClient(Config &config, ComponentApp *manager) {
    OpenSslClient *client = new OpenSslClient((unsigned long) config.getIntValue(PROPERTY_MODULE_OPENSSL_ID),
                                              config.getIntValue(PROPERTY_MODULE_OPENSSL_LOCAL_PORT),
                                              config.getStringValue(PROPERTY_MODULE_OPENSSL_CERTIFICATE),
                                              config.getStringValue(PROPERTY_MODULE_OPENSSL_KEY),
                                              config.getStringValue(PROPERTY_MODULE_OPENSSL_REMOTE_IP),
                                              config.getIntValue(PROPERTY_MODULE_OPENSSL_REMOTE_PORT));

    client->start(manager);
    std::this_thread::sleep_for(std::chrono::seconds(1));
    client->joinProtocol();
    return client;
}

int main(int args, char **argv) {
    if (args != 3) {
        logger::error(APP_NAME, "Usage: " + string(argv[0]) + " <project conf file> <conf profile>");
        return 1;
    }

    Config config;
    if (config.load(argv[1])) {
        if (config.setSection(argv[2], false)) {
            // create a generic component application that prints every message received
            ComponentApp *componentApp = new ComponentApp();
            GenericClient *client = initClient(config, componentApp);

            std::this_thread::sleep_for(std::chrono::seconds(1));

            string line = " ";
            while (!line.empty()) {
                logger::info(APP_NAME, "Enter your message (an empty line will end the program): ");
                getline(cin, line);
                if (!line.empty()) {
                    // the message is a concatenation of a random int + the line read from stdin
                    Message *message = new SimpleMessage(rand(), line);
                    client->publish(*message);
                    delete message;
                }
            }

            client->close();
            delete client;
            delete componentApp;
            return 0;
        } else {
            logger::error(APP_NAME, "Could not load configuration profile: " + string(argv[2]));
            return 1;
        }
    } else {
        logger::error(APP_NAME, "Could not load configuration file: " + string(argv[1]));
        return 1;
    }
}