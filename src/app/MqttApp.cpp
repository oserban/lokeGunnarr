//
// Created by ovidiu on 13/10/15.
//

#include <iostream>
#include <rude/config.h>
#include "ComponentApp.h"
#include "../protocol/client/GenericClient.h"
#include "../message/SimpleMessage.h"
#include "../protocol/client/MqttClient.h"
#include "../helper/Helper.h"

#define APP_NAME "MQTT App"
#define PROPERTY_MODULE_MQTT_ID "module.mqtt.id"
#define PROPERTY_MODULE_MQTT_IP "module.mqtt.broker.ip"
#define PROPERTY_MODULE_MQTT_PORT "module.mqtt.broker.port"

using namespace std;
using namespace rude;

/*
 * The Mosquito (MQTT) App, which connects to the broker
 */

GenericClient *initClient(Config &config, ComponentApp *manager) {
    GenericClient *client = new MqttClient((unsigned long) config.getIntValue(PROPERTY_MODULE_MQTT_ID),
                                           config.getStringValue(PROPERTY_MODULE_MQTT_IP),
                                           config.getIntValue(PROPERTY_MODULE_MQTT_PORT));
    client->start(manager);
    return client;
}

int main(int args, char **argv) {
    if (args != 3) {
        logger::error(APP_NAME, "Usage: " + string(argv[0]) + " <project conf file> <conf profile>");
        return 1;
    }

    Config config;
    if (config.load(argv[1])) {
        if (config.setSection(argv[2], false)) {
            // create a generic component application that prints every message received
            ComponentApp *componentApp = new ComponentApp();
            GenericClient *client = initClient(config, componentApp);

            std::this_thread::sleep_for(std::chrono::seconds(1));

            string line = " ";
            while (!line.empty()) {
                logger::info(APP_NAME, "Enter your message (an empty line will end the program): ");
                getline(cin, line);
                if (!line.empty()) {
                    // the message is a concatenation of a random int + the line read from stdin
                    Message *message = new SimpleMessage(rand(), line);
                    client->publish(*message);
                    delete message;
                }
            }

            client->close();
            delete client;
            delete componentApp;
            return 0;
        } else {
            logger::error(APP_NAME, "Could not load configuration profile: " + string(argv[2]));
            return 1;
        }
    } else {
        logger::error(APP_NAME, "Could not load configuration file: " + string(argv[1]));
        return 1;
    }
}
