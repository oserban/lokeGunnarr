//
// Created by ovidiu on 13/10/15.
//

#ifndef LOKEGUNNARR_COMPONENTAPP_H
#define LOKEGUNNARR_COMPONENTAPP_H


#include "../protocol/ProtocolListener.h"

// Basic component application, currently logs all the received messages
class ComponentApp : public ProtocolListener{
    void messageReceived(Message &message);
};


#endif //LOKEGUNNARR_COMPONENTAPP_H
