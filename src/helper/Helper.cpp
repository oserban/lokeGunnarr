//
// Created by ovidiu on 13/10/15.
//

#include <iostream>
#include "Helper.h"

using namespace std;

string logger::getDateTime() {
    time_t rawtime;
    struct tm *timeinfo;
    char buffer[80];

    time(&rawtime);
    timeinfo = localtime(&rawtime);

    strftime(buffer, 80, "%d-%m-%Y %I:%M:%S", timeinfo);
    std::string str(buffer);

    return str;
}

void logger::logMessage(std::string level, std::string module, std::string message) {
    auto dateTime = getDateTime();
    cout << level + " [" + dateTime + "] [" + module + "] " + message + "\n";
}

void logger::info(std::string module, std::string message) {
    logMessage("INFO ", module, message);
}

void logger::debug(std::string module, std::string message) {
    if (DEBUG_ENABLED) {
        logMessage("DEBUG", module, message);
    }
}

void logger::error(std::string module, std::string message) {
    logMessage("ERROR", module, message);
}

bool file::fileExists(std::string filename) {
    if (FILE *file = fopen(filename.data(), "r")) {
        fclose(file);
        return true;
    } else {
        return false;
    }
}