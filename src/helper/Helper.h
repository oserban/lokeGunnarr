//
// Created by ovidiu on 13/10/15.
//

#ifndef LOKEGUNNARR_HELPER_H
#define LOKEGUNNARR_HELPER_H

#define DEBUG_ENABLED true

#include <sstream>

/*
 * Several helper methods for logging and checking if a file can be read properly
 * ----
 * Set DEBUG_ENABLED to false to disable debug level in all the project
 */

namespace logger {
    std::string getDateTime();

    void logMessage(std::string level, std::string module, std::string message);

    void info(std::string module, std::string message);

    void debug(std::string module, std::string message);

    void error(std::string module, std::string message);
}

namespace file {
    bool fileExists(std::string filename);
}
#endif