//
// Created by ovidiu on 13/10/15.
//

#include "SimpleMessage.h"

SimpleMessage::SimpleMessage(int number, const std::string &information) :
        Message(0), number(number), information(information) { }

SimpleMessage::SimpleMessage(std::string &representation) : Message(representation) {
    unsigned long pos = representation.find_first_of(" ", 0);
    if (pos != std::string::npos) {
        this->number = atoi(representation.substr(0, pos).data());
    } else {
        this->number = 0;
    }
    pos++;

    this->information = representation.substr(pos, representation.size() - pos);
}

Message *SimpleMessage::clone() {
    SimpleMessage *simpleMessage = new SimpleMessage(this->number, this->information);
    simpleMessage->setSourceId(this->getSourceId());
    return simpleMessage;
}

MessageType SimpleMessage::getType() {
    return simple;
}

std::string SimpleMessage::serialiseDelegate() {
    std::stringstream ss;
    ss << number << " " << information;
    return ss.str();
}

std::string SimpleMessage::toString() {
    std::stringstream ss;
    ss << number << " " << information;
    return ss.str();
}
