//
// Created by ovidiu on 14/10/15.
//

#include "ProtocolCommand.h"

std::string getCommandName(ProtocolCommand protocolCommand) {
    switch (protocolCommand) {
        case join:
            return "protocol_join";
        case reject:
            return "protocol_reject";
        case ack:
            return "protocol_ack";
        case leave:
            return "protocol_leave";
        default:
            return "";
    }
}