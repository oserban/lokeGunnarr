//
// Created by ovidiu on 14/10/15.
//

#ifndef LOKEGUNNARR_PROTOCOLMESSAGE_H
#define LOKEGUNNARR_PROTOCOLMESSAGE_H


#include "Message.h"
#include "ProtocolCommand.h"

/*
 * A message wrapper of several protocol commands.
 *  - for the join command, the hostname (ip) and port of the client joining is provided
 *  - for any other command, the host and port are not used (empty)
 */

class ProtocolMessage : public Message {
private:
    ProtocolCommand protocolCommand;
    std::string hostname;
    int port;
public:
    ProtocolMessage(ProtocolCommand protocolCommand)
            : Message(0), protocolCommand(protocolCommand), hostname(""), port(-1) { }

    ProtocolMessage(ProtocolCommand protocolCommand, std::string hostname, int port)
            : Message(0), protocolCommand(protocolCommand), hostname(hostname), port(port) { }

    ProtocolMessage(std::string &representation);

    virtual ~ProtocolMessage() { }

    const std::string &getHostname() const {
        return hostname;
    }

    int getPort() const {
        return port;
    }

    virtual Message *clone();

    virtual MessageType getType();

    const ProtocolCommand &getProtocolCommand() const {
        return protocolCommand;
    }

    virtual std::string serialiseDelegate();

    virtual std::string toString();
};


#endif //LOKEGUNNARR_PROTOCOLMESSAGE_H
