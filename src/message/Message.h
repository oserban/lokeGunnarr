//
// Created by ovidiu on 12/10/15.
//

#ifndef LOKEGUNNARR_MESSAGE_H
#define LOKEGUNNARR_MESSAGE_H


#include <string>
#include <sstream>
#include "MessageType.h"

/*
 * Base class of a message:
 *  - contains a source id to uniquely identify the origin of a message (used in broadcast filtering)
 *  - a message can be cloned, serialised and de-serialised from a given representation
 */

class Message {
private:
    unsigned long sourceId;
public:
    Message(unsigned long sourceId) : sourceId(sourceId) { }

    Message(std::string &representation) {};

    virtual ~Message() { }


    unsigned long getSourceId() const {
        return sourceId;
    }

    void setSourceId(unsigned long sourceId) {
        Message::sourceId = sourceId;
    }

    virtual Message* clone() = 0;

    virtual MessageType getType() = 0;

    std::string serialise();

    virtual std::string toString() = 0;

    static Message *create(void *representation, unsigned long size);

protected:
    virtual std::string serialiseDelegate() = 0;
};


#endif //LOKEGUNNARR_MESSAGE_H
