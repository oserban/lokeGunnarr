//
// Created by ovidiu on 14/10/15.
//

#ifndef LOKEGUNNARR_PROTOCOLCOMMAND_H
#define LOKEGUNNARR_PROTOCOLCOMMAND_H


#include <string>

/*
 * Several protocol commands, used in OpenSSL client management
 *
 * The index of the elements has to be strict, since the information is serialised within the message format.
 */
enum ProtocolCommand {
    join = 1,
    leave = 2,
    ack = 3,
    reject = 4
};


std::string getCommandName(ProtocolCommand protocolCommand);

#endif //LOKEGUNNARR_PROTOCOLCOMMAND_H
