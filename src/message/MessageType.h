//
// Created by ovidiu on 13/10/15.
//

#ifndef LOKEGUNNARR_MESSAGETYPE_H
#define LOKEGUNNARR_MESSAGETYPE_H

/*
 * Message types are declared here. Currently a message can be either simple (containing a number and a string)
 * or protocol (used in OpenSSL management).
 *
 * The index of the elements has to be strict, since the information is serialised within the message format.
 */

enum MessageType {
    simple = 1,
    protocol = 2
};


#endif //LOKEGUNNARR_MESSAGETYPE_H
