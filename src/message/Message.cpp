//
// Created by ovidiu on 13/10/15.
//

#include  "Message.h"
#include "SimpleMessage.h"
#include "ProtocolMessage.h"

using namespace std;

Message *Message::create(void *representation, unsigned long size) {
    unsigned long sourceId = 0;
    int messageType = 0;

    string ss = string((char *) representation, size);

    unsigned long pos = ss.find_first_of(" ", 0);
    if (pos != std::string::npos) {
        messageType = atoi(ss.substr(0, pos).data());
    }
    pos++;
    ss = ss.substr(pos, ss.size() - pos);

    pos = ss.find_first_of(" ", 0);
    if (pos != std::string::npos) {
        sourceId = strtoul(ss.substr(0, pos).data(), NULL, 0);
    }
    pos++;
    ss = ss.substr(pos, ss.size() - pos);

    MessageType type = static_cast<MessageType>(messageType);
    Message *result = NULL;
    if (type == simple) {
        result = new SimpleMessage(ss);
    } else if (type == protocol) {
        result = new ProtocolMessage(ss);
    }

    if (result != NULL) {
        result->setSourceId(sourceId);
    }
    return result;
}


std::string Message::serialise() {
    std::stringstream ss;
    ss << getType() << " " << sourceId << " " << serialiseDelegate();
    return ss.str();
}
