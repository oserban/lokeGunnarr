//
// Created by ovidiu on 14/10/15.
//

#include "ProtocolMessage.h"

using namespace std;


ProtocolMessage::ProtocolMessage(std::string &representation) : Message(representation) {
    unsigned long pos = representation.find_first_of(" ", 0);
    if (pos != std::string::npos) {
        protocolCommand = static_cast<ProtocolCommand>(atoi(representation.substr(0, pos).data()));
        pos++;
        unsigned long xpos = representation.find_first_of(":", pos);
        hostname = representation.substr(pos, xpos - pos);
        xpos++;
        port = atoi(representation.substr(xpos, representation.size() - xpos).data());
    } else {
        protocolCommand = static_cast<ProtocolCommand>(atoi(representation.data()));
        hostname = "";
        port = -1;
    }
}

Message *ProtocolMessage::clone() {
    ProtocolMessage *simpleMessage = new ProtocolMessage(this->protocolCommand);
    simpleMessage->setSourceId(this->getSourceId());
    return simpleMessage;
}

MessageType ProtocolMessage::getType() {
    return protocol;
}

std::string ProtocolMessage::serialiseDelegate() {
    std::stringstream ss;
    ss << to_string(static_cast<int>(protocolCommand));
    if (port > 0) {
        ss << " " << hostname << ":" << port;
    }
    return ss.str();
}

std::string ProtocolMessage::toString() {
    std::stringstream ss;
    ss << getCommandName(protocolCommand);
    if (port > 0) {
        ss << " " << hostname << ":" << port;
    }
    return ss.str();
}


