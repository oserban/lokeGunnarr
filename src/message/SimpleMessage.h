//
// Created by ovidiu on 13/10/15.
//

#ifndef LOKEGUNNARR_SIMPLEMESSAGE_H
#define LOKEGUNNARR_SIMPLEMESSAGE_H

#include <string>
#include "MessageType.h"
#include "Message.h"

/*
 * A simple message contains a number a and a text (information).
 * It is the most commonly used message structure in the project.
 */

class SimpleMessage : public Message {
private:
    int number;
    std::string information;
public:
    SimpleMessage(int number, const std::string &information);
    SimpleMessage(std::string &representation);

    int getNumber() const {
        return number;
    }

    void setNumber(int number) {
        SimpleMessage::number = number;
    }

    const std::string &getInformation() const {
        return information;
    }

    void setInformation(const std::string &information) {
        SimpleMessage::information = information;
    }

    virtual Message* clone();

    virtual MessageType getType();

    virtual std::string toString();

protected:
    virtual std::string serialiseDelegate();
};


#endif //LOKEGUNNARR_SIMPLEMESSAGE_H
