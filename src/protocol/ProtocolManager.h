//
// Created by ovidiu on 12/10/15.
//

#ifndef LOKEGUNNARR_PROTOCOLMANAGER_H
#define LOKEGUNNARR_PROTOCOLMANAGER_H


#include "ProtocolListener.h"
#include "module/GenericModule.h"
#include "module/ModuleType.h"
#include "Publisher.h"
#include "GenericProcessingStrategy.h"
#include <list>

/*
 * The protocol manager handles all the modules added to a core app
 * It also listens for any new messages received and triggers the processing strategy, if set
 *
 * The class acts as a publisher of messages, in which case it broadcasts the message to all the configured modules.
 */

class ProtocolManager : public ProtocolListener, public Publisher {
private:
    std::list<GenericModule *> modules;
    GenericProcessingStrategy *processingStrategy;
    unsigned long managerId;
public:
    ProtocolManager(unsigned long managerId, GenericProcessingStrategy *processingStrategy)
            : managerId(managerId), processingStrategy(processingStrategy) { }

    virtual ~ProtocolManager() { delete processingStrategy; }

    void addZeroMqModule(std::string protocol, int subscribePort, int publishPort);

    void addMqttModule(std::string brokerHostname, int brokerPort);

    void addOpenSslModule(int listeningPort, std::string certFile, std::string keyFile);

    void removeModule(ModuleType module);

    void messageReceived(Message &message) override;

    void start();

    void close();

    virtual void publish(Message &message) override;
};

#endif //LOKEGUNNARR_PROTOCOLMANAGER_H
