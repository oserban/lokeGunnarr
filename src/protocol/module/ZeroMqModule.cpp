//
// Created by ovidiu on 12/10/15.
//

#include <sstream>
#include "ZeroMqModule.h"
#include "../../helper/Helper.h"

using namespace zmq;
using namespace std;

ZeroMqModule::ZeroMqModule(string protocol, int subscribePort, int publishPort) {
    context = new context_t(1);
    subscribeSocket = new socket_t(*context, ZMQ_SUB);
    publishSocket = new socket_t(*context, ZMQ_PUB);

    this->protocol = protocol;
    this->subscribePort = subscribePort;
    this->publishPort = publishPort;
}

ZeroMqModule::~ZeroMqModule() {
    delete subscribeSocket;
    delete publishSocket;
}


string getBindAddress(string protocol, int port) {
    stringstream ss;
    ss << protocol << "://*:" << port;
    return ss.str();
}

void ZeroMqModule::run(ProtocolListener *protocolListener) {
    if ((subscribeSocket != NULL) && (protocolListener != NULL)) {
        string bindAddress = getBindAddress(protocol, subscribePort);
        logger::info("ZeroMQ", "Creating subscribe address: " + bindAddress);
        subscribeSocket->bind(bindAddress);
        subscribeSocket->setsockopt(ZMQ_SUBSCRIBE, "", 0);

        bindAddress = getBindAddress(protocol, publishPort);
        logger::info("ZeroMQ", "Creating publish address: " + bindAddress);
        publishSocket->bind(bindAddress);

        while (isRunning()) {
            zmq::message_t request;

            //  Wait for next request from client
            subscribeSocket->recv(&request);
            if ((subscribeSocket != NULL) && (subscribeSocket->connected())) {
                Message *message = Message::create(request.data(), request.size());
                protocolListener->messageReceived(*message);
                delete message;
            }
        }
    }
}

void ZeroMqModule::publish(Message &message) {
    string representation = message.serialise();
    publishSocket->send(representation.data(), representation.size(), 0);
}

ModuleType ZeroMqModule::getType() {
    return ZeroMQ;
}

void ZeroMqModule::closeDelegate() { }
