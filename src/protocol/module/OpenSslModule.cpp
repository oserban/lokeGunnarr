//
// Created by ovidiu on 14/10/15.
//

#include <sys/socket.h>
#include <openssl/err.h>
#include <netinet/in.h>
#include <unistd.h>
#include "OpenSslModule.h"
#include "../../helper/Helper.h"
#include "../client/SimpleOpenSslClient.h"

using namespace std;

OpenSslModule::OpenSslModule(int port, std::string certFile, std::string keyFile) {
    this->port = port;
    ctx = initServerCTX();
    loadCertificates(ctx, certFile, keyFile);
}

OpenSslModule::~OpenSslModule() {
    SSL_CTX_free(ctx);
}

ModuleType OpenSslModule::getType() {
    return OpenSSL;
}

void OpenSslModule::run(ProtocolListener *protocolListener) {
    serverHandler = openServerListener(port);
    logger::info("OpenSSL", "OpenSSL Server started on *:" + to_string(port));

    while (isRunning()) {
        struct sockaddr_in addr;
        socklen_t len = sizeof(sockaddr_in);
        SSL *ssl;

        int client = accept(serverHandler, (struct sockaddr *) &addr, &len);
        if (isRunning()) {
            ssl = SSL_new(ctx);
            SSL_set_fd(ssl, client);
            onConnection(ssl, protocolListener);
        }
    }
    ::close(serverHandler);
}

void OpenSslModule::closeDelegate() {
    ::close(serverHandler);
}

int OpenSslModule::openServerListener(int port) {
    int sd;
    struct sockaddr_in addr;

    sd = socket(PF_INET, SOCK_STREAM, 0);
    bzero(&addr, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = INADDR_ANY;
    if (::bind(sd, (struct sockaddr *) &addr, sizeof(addr)) != 0) {
        logger::error("OpenSSL", "Can't bind port");
        abort();
    }
    if (listen(sd, 10) != 0) {
        logger::error("OpenSSL", "Can't configure listening port");
        abort();
    }
    return sd;
}

SSL_CTX *OpenSslModule::initServerCTX() {
    const SSL_METHOD *method;
    SSL_CTX *ctx;

    SSL_library_init();
    OpenSSL_add_all_algorithms();
    SSL_load_error_strings();
    method = SSLv3_server_method();
    ctx = SSL_CTX_new(method);
    if (ctx == NULL) {
        logger::error("OpenSSL", "Invalid context descriptor!");
        abort();
    }
    return ctx;
}

void OpenSslModule::loadCertificates(SSL_CTX *ctx, std::string certFile, std::string keyFile) {
    if (SSL_CTX_use_certificate_file(ctx, certFile.data(), SSL_FILETYPE_PEM) <= 0) {
        logger::error("OpenSSL", "Invalid certificate!");
        ERR_print_errors_fp(stderr);
        abort();
    }
    /* set the private key from KeyFile (may be the same as CertFile) */
    if (SSL_CTX_use_PrivateKey_file(ctx, keyFile.data(), SSL_FILETYPE_PEM) <= 0) {
        logger::error("OpenSSL", "Invalid private key!");
        ERR_print_errors_fp(stderr);
        abort();
    }
    /* verify private key */
    if (!SSL_CTX_check_private_key(ctx)) {
        logger::error("OpenSSL", "Private key does not match the public certificate!");
        abort();
    }
}

void OpenSslModule::onConnection(SSL *ssl, ProtocolListener *protocolListener) {
    char buf[1024];
    int sd, bytes;

    if (SSL_accept(ssl) == -1) {
        logger::error("OpenSSL", "Failed to accept the SSL certificate !");
    } else {
        // optimistic parse of the message
        bytes = SSL_read(ssl, buf, sizeof(buf));
        if (bytes > 0) {
            buf[bytes] = 0;
            Message *message = Message::create(buf, (unsigned long) bytes);
            if (message->getType() == protocol) {
                processProtocol((ProtocolMessage *) message, ssl);
            } else if (isConnected(message->getSourceId())) {
                protocolListener->messageReceived(*message);
                Message *result = new ProtocolMessage(ack);
                sslWrite(result, ssl);
                delete result;
            } else {
                // reject message
                Message *result = new ProtocolMessage(reject);
                sslWrite(result, ssl);
                delete result;
            }
            delete message;
        }
    }
    sd = SSL_get_fd(ssl);
    SSL_free(ssl);
    ::close(sd);
}

void OpenSslModule::publish(Message &message) {
    if (connectedApps.empty()) {
        logger::debug("OpenSSL", "Connection map is empty");
    } else {
        for (map<unsigned long, pair<string, int>>::iterator it = connectedApps.begin();
             it != connectedApps.end(); ++it) {
            if (message.getSourceId() != it->first) {
                pair<string, int> hostPort = it->second;
                ProtocolMessage reply = SimpleOpenSslClient::sendSslMessage(hostPort.first, hostPort.second, &message);
                if (reply.getProtocolCommand() != ack) {
                    // target unreachable => leave protocol
                    connectedApps.erase(it->first);
                    logger::debug("OpenSSL", "Host unavailable, removed pair: " + hostPort.first + ":" +
                                             to_string(hostPort.second));
                } else {
                    logger::debug("OpenSSL",
                                  "Forwarded message to: " + hostPort.first + ":" + to_string(hostPort.second));
                }
            }
        }
    }
}

void OpenSslModule::sslWrite(Message *message, SSL *ssl) {
    string serialisation = message->serialise();
    SSL_write(ssl, serialisation.data(), (int) serialisation.size());
}

void OpenSslModule::processProtocol(ProtocolMessage *message, SSL *ssl) {
    logger::debug("OpenSSL", "Received protocol message: " + message->toString());

    bool success = false;

    if (message->getProtocolCommand() == join) {
        connectedApps[message->getSourceId()] = pair<string, int>(message->getHostname(), message->getPort());
        success = true;
    } else if (message->getProtocolCommand() == leave) {
        connectedApps.erase(message->getSourceId());
        success = true;
    }

    if (success) {
        Message *result = new ProtocolMessage(ack);
        sslWrite(result, ssl);
        delete result;
    } else {
        Message *result = new ProtocolMessage(reject);
        sslWrite(result, ssl);
        delete result;
    }
}

bool OpenSslModule::isConnected(unsigned long sourceId) {
    return connectedApps.count(sourceId) > 0;
}
