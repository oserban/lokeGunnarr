//
// Created by ovidiu on 12/10/15.
//

#ifndef LOKEGUNNARR_GENERICMODULE_H
#define LOKEGUNNARR_GENERICMODULE_H


#include <thread>
#include "ModuleType.h"
#include "../ProtocolListener.h"

/*
 * A module implements a communication protocol and it is able to handle and publish messages.
 * The module also runs on its own thread.
 */

class GenericModule {
private:
    bool running = true;
    std::thread *localThread;
public:
    virtual ~GenericModule() { }

    virtual void publish(Message &message) = 0;

    void start(ProtocolListener *protocolListener) {
        localThread = new std::thread(&GenericModule::run, this, protocolListener);
        localThread->detach();
    };

    void close() {
        this->running = false;
        closeDelegate();
        if (localThread != NULL) {
            delete localThread;
        }
    }

    virtual ModuleType getType() = 0;

protected:
    virtual void run(ProtocolListener *protocolListener) = 0;

    bool isRunning() const {
        return running;
    }

    virtual void closeDelegate() = 0;
};


#endif //LOKEGUNNARR_GENERICMODULE_H
