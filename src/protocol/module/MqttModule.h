//
// Created by ovidiu on 13/10/15.
//

#ifndef LOKEGUNNARR_MQTTMODULE_H
#define LOKEGUNNARR_MQTTMODULE_H

#define CH_COMPONENT "mqtt/component"
#define CH_CORE_APP "mqtt/coreApp"

#include <mosquittopp.h>
#include "GenericModule.h"

/*
 * A module implementation for the Mosquito protocol.
 *
 * This is very similar to the client implementation, since the MQTT uses a broker mediated connection.
 */

class MqttModule : public GenericModule, public mosqpp::mosquittopp {
private:
    ProtocolListener *protocolListener;
    std::string brokerHostname;
    int brokerPort;
public:
    MqttModule(unsigned long moduleId, const std::string &brokerHostname, int brokerPort)
            : mosquittopp(std::to_string(moduleId).data()), brokerHostname(brokerHostname), brokerPort(brokerPort) {
        mosqpp::lib_init();
    }

    virtual ~MqttModule() {
        mosqpp::lib_cleanup();
    }

    virtual void on_connect(int rc) override;

    virtual void on_message(const struct mosquitto_message *message) override;

    virtual void on_log(int level, const char *str) override;

    virtual void on_error() override;

    virtual void publish(Message &message) override;

    virtual ModuleType getType() override;

protected:
    virtual void run(ProtocolListener *protocolListener) override;

    virtual void closeDelegate() override;
};


#endif //LOKEGUNNARR_MQTTMODULE_H
