//
// Created by ovidiu on 13/10/15.
//

#include <string.h>
#include "MqttModule.h"
#include "../../helper/Helper.h"

using namespace std;

void MqttModule::publish(Message &message) {
    string serialization = message.serialise();
    const char *buf = serialization.data();
    mosqpp::mosquittopp::publish(NULL, CH_CORE_APP, (int) strlen(buf), buf);
    logger::info("MQTT", "Message sent:" + serialization);
}

ModuleType MqttModule::getType() {
    return MQTT;
}

void MqttModule::run(ProtocolListener *protocolListener) {
    this->protocolListener = protocolListener;
    connect(brokerHostname.data(), brokerPort, 60);
    logger::info("MQTT", "Connecting to broker: " + brokerHostname + ":" + to_string(brokerPort));
    while (isRunning()) {
        int rc = loop();
        if (rc) {
            reconnect();
        }
    }
}

void MqttModule::closeDelegate() {
    disconnect();
}

void MqttModule::on_connect(int rc) {
    if (rc == 0) {
        subscribe(NULL, CH_COMPONENT, 0);
    }
}

void MqttModule::on_message(const struct mosquitto_message *message) {
    if (protocolListener != NULL) {
        Message *m = Message::create(message->payload, (unsigned long) message->payloadlen);
        protocolListener->messageReceived(*m);
        delete m;
    }
}

void MqttModule::on_log(int level, const char *str) {
    logger::debug("MQTT", "Library Log message:" + string(str));
}

void MqttModule::on_error() {
    logger::debug("MQTT", "Library error!");
}
