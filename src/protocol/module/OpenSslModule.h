//
// Created by ovidiu on 14/10/15.
//

#ifndef LOKEGUNNARR_OPENSSLMODULE_H
#define LOKEGUNNARR_OPENSSLMODULE_H


#include "GenericModule.h"
#include "../../message/ProtocolMessage.h"
#include <openssl/ssl.h>
#include <string>
#include <map>

/*
 * The OpenSSL implementation of a module. Can handle multiple SSL socket connections at the time
 * and the connected clients need to send a join command first. Sending any message without joining first is
 * rejected by this module.
 *
 * The module manages the client connections and it is able to reconnect based on their provided credentials.
 * In case the client is not accessible after the first connection, the module consider the client outside of
 * the network and triggers the leaving protocol automatically.
 */


class OpenSslModule : public GenericModule {
private:
    int port;
    SSL_CTX *ctx;
    int serverHandler;
    std::map<unsigned long, std::pair<std::string, int>> connectedApps;
public:
    OpenSslModule(int port, std::string certFile, std::string keyFile);

    virtual ~OpenSslModule();

    virtual void publish(Message &message) override;

    virtual ModuleType getType() override;

protected:
    virtual void run(ProtocolListener *protocolListener) override;

    virtual void closeDelegate() override;

private:
    int openServerListener(int port);

    SSL_CTX *initServerCTX();

    void loadCertificates(SSL_CTX *ctx, std::string certFile, std::string keyFile);

    void onConnection(SSL *ssl, ProtocolListener *protocolListener);

    // helper methods
    void processProtocol(ProtocolMessage *message, SSL *ssl);

    bool isConnected(unsigned long sourceId);

    void sslWrite(Message *message, SSL *ssl);
};


#endif //LOKEGUNNARR_OPENSSLMODULE_H
