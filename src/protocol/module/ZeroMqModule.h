//
// Created by ovidiu on 12/10/15.
//

#ifndef LOKEGUNNARR_ZEROMQSERVER_H
#define LOKEGUNNARR_ZEROMQSERVER_H


#include "GenericModule.h"
#include "../../../lib/zmq/zmq.hpp"

/*
 * The ZeroMQ implementation of the module.
 * The server is accepting connection, therefore the hostname is not needed for both the publishing and subscribing sockets.
 *
 * The client and the module uses a publish/subscribe communication paradigm
 */

class ZeroMqModule : public GenericModule {
private:
    zmq::context_t *context;
    zmq::socket_t *subscribeSocket;
    zmq::socket_t *publishSocket;

    std::string protocol;
    int publishPort;
    int subscribePort;

public:
    ZeroMqModule(std::string protocol, int subscribePort, int publishPort);

    virtual ~ZeroMqModule();

private:
    void run(ProtocolListener *protocolListener) override;

    ModuleType getType() override;

    void publish(Message& message) override;


protected:
    virtual void closeDelegate() override;
};


#endif //LOKEGUNNARR_ZEROMQSERVER_H
