//
// Created by ovidiu on 13/10/15.
//

#include "ModuleType.h"

std::string getModuleName(ModuleType type) {
    switch (type) {
        case ZeroMQ:
            return "ZeroMQ";
        case MQTT:
            return "MQTT";
        case OpenSSL:
            return "OpenSSL";
    }
    return "";
}
