//
// Created by ovidiu on 12/10/15.
//

#ifndef LOKEGUNNARR_MODULE_H
#define LOKEGUNNARR_MODULE_H


#include <string>

/*
 * Various module types can be added in this class.
 * The index of the elements is not strict since the information is used locally.
 */

enum ModuleType {
    ZeroMQ,
    MQTT,
    OpenSSL
};


std::string getModuleName(ModuleType type);

#endif //LOKEGUNNARR_MODULE_H
