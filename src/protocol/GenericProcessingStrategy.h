//
// Created by ovidiu on 13/10/15.
//

#ifndef LOKEGUNNARR_GENERICPROCESSINGSTRATEGY_H
#define LOKEGUNNARR_GENERICPROCESSINGSTRATEGY_H


#include "../message/Message.h"
#include "Publisher.h"

/*
 * A processing strategy is triggered by the protocol manager on every new message.
 * A strategy can be extended and set at construction time of the manager.
 */

class GenericProcessingStrategy {
public:
    virtual ~GenericProcessingStrategy() { }

    virtual void process(Message &message, Publisher *publisher) = 0;
};


#endif //LOKEGUNNARR_GENERICPROCESSINGSTRATEGY_H
