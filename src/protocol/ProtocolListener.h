//
// Created by ovidiu on 12/10/15.
//

#ifndef LOKEGUNNARR_PROTOCOLLISTENER_H
#define LOKEGUNNARR_PROTOCOLLISTENER_H


#include "../message/Message.h"

/*
 * This is an abstract protocol listener which is usually triggered once every new message is received.
 * Multiple classes implement this behaviour such as: the protocol manager and component app
 */

class ProtocolListener {
public:
    virtual void messageReceived(Message &message) = 0;
};


#endif //LOKEGUNNARR_PROTOCOLLISTENER_H
