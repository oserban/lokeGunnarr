//
// Created by ovidiu on 13/10/15.
//

#ifndef LOKEGUNNARR_MQTTCLIENT_H
#define LOKEGUNNARR_MQTTCLIENT_H
// the component channel is used by any client to send messages to the Core App
#define CH_COMPONENT "mqtt/component"
// the core App channel is used to broadcast all the messages from the Core App
#define CH_CORE_APP "mqtt/coreApp"

#include <mosquittopp.h>
#include "GenericClient.h"

/*
 * A Client implementation for the Mosquito protocol.
 *
 * This is very similar to the module implementation, since the MQTT uses a broker mediated connection.
 */

class MqttClient : public GenericClient, public mosqpp::mosquittopp {
private:
    ProtocolListener *protocolListener;
    std::string brokerHostname;
    int brokerPort;
public:
    MqttClient(unsigned long clientId, std::string brokerHostname, int brokerPort)
            : mosquittopp(std::to_string(clientId).data()), GenericClient(clientId),
              brokerHostname(brokerHostname), brokerPort(brokerPort) {
        mosqpp::lib_init();
    }

    virtual ~MqttClient() {
        mosqpp::lib_cleanup();
    }

    virtual void on_connect(int rc) override;

    virtual void on_message(const struct mosquitto_message *message) override;

    virtual void on_log(int level, const char *str) override;

    virtual void on_error() override;

    virtual void publish(Message &message) override;

protected:
    virtual void run(ProtocolListener *protocolListener) override;

    virtual void closeDelegate() override;
};


#endif //LOKEGUNNARR_MQTTCLIENT_H
