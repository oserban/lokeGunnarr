//
// Created by ovidiu on 14/10/15.
//

#ifndef LOKEGUNNARR_SIMPLEOPENSSLCLIENT_H
#define LOKEGUNNARR_SIMPLEOPENSSLCLIENT_H


#include "../../message/ProtocolMessage.h"
/*
 * Simple helper class used to send a single message on an SSL enabled connection
 */
class SimpleOpenSslClient {
public:
    static ProtocolMessage sendSslMessage(std::string hostname, int port, Message *message);
};


#endif //LOKEGUNNARR_SIMPLEOPENSSLCLIENT_H
