//
// Created by ovidiu on 12/10/15.
//

#ifndef LOKEGUNNARR_GENERICCLIENT_H
#define LOKEGUNNARR_GENERICCLIENT_H


#include <stddef.h>
#include <thread>
#include "../../message/Message.h"
#include "../ProtocolListener.h"
#include "../Publisher.h"

/*
 * A client can communicate with a module within the Core App.
 * Each client runs on a different thread, managed locally.
 * The client id uniquely identifies the client connection to the Core App
 */

class GenericClient : public Publisher {
private:
    bool running = true;
    std::thread *localThread;
protected:
    unsigned long clientId;
public:
    GenericClient(unsigned long clientId) : clientId(clientId) { }

    virtual ~GenericClient() { }

    unsigned long getClientId() const {
        return clientId;
    }

    void setClientId(unsigned long clientId) {
        GenericClient::clientId = clientId;
    }

    virtual void publish(Message &message) = 0;

    void start(ProtocolListener *protocolListener) {
        localThread = new std::thread(&GenericClient::run, this, protocolListener);
        localThread->detach();
    };

    void close() {
        this->running = false;
        closeDelegate();
        if (localThread != NULL) {
            delete localThread;
        }
    }

protected:
    virtual void run(ProtocolListener *protocolListener) = 0;

    bool isRunning() const {
        return running;
    }

    virtual void closeDelegate() = 0;
};


#endif //LOKEGUNNARR_GENERICCLIENT_H
