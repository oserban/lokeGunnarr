//
// Created by ovidiu on 14/10/15.
//

#include <sys/socket.h>
#include <openssl/err.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <ifaddrs.h>
#include "OpenSslClient.h"
#include "../../helper/Helper.h"
#include "SimpleOpenSslClient.h"

using namespace std;

OpenSslClient::OpenSslClient(unsigned long clientId, int localPort, std::string certFile, std::string keyFile,
                             std::string remoteHost, int remotePort)
        : GenericClient(clientId), localPort(localPort), remoteHost(remoteHost), remotePort(remotePort) {
    ctx = initServerCTX();
    loadCertificates(ctx, certFile, keyFile);
}

OpenSslClient::~OpenSslClient() {
    SSL_CTX_free(ctx);
}

void OpenSslClient::run(ProtocolListener *protocolListener) {
    serverHandler = openServerListener(localPort);
    logger::info("OpenSSL", "OpenSSL Server started on *:" + to_string(localPort));

    while (isRunning()) {
        struct sockaddr_in addr;
        socklen_t len = sizeof(sockaddr_in);
        SSL *ssl;

        int client = accept(serverHandler, (struct sockaddr *) &addr, &len);
        if (isRunning()) {
            ssl = SSL_new(ctx);
            SSL_set_fd(ssl, client);
            onConnection(ssl, protocolListener);
        }
    }
    ::close(serverHandler);
}


void OpenSslClient::closeDelegate() {
    ::close(serverHandler);
}

int OpenSslClient::openServerListener(int port) {
    int sd;
    struct sockaddr_in addr;

    sd = socket(PF_INET, SOCK_STREAM, 0);
    bzero(&addr, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = INADDR_ANY;
    if (::bind(sd, (struct sockaddr *) &addr, sizeof(addr)) != 0) {
        logger::error("OpenSSL", "Can't bind port");
        abort();
    }
    if (listen(sd, 10) != 0) {
        logger::error("OpenSSL", "Can't configure listening port");
        abort();
    }
    return sd;
}

SSL_CTX *OpenSslClient::initServerCTX() {
    const SSL_METHOD *method;
    SSL_CTX *ctx;

    SSL_library_init();
    OpenSSL_add_all_algorithms();
    SSL_load_error_strings();
    method = SSLv3_server_method();
    ctx = SSL_CTX_new(method);
    if (ctx == NULL) {
        logger::error("OpenSSL", "Invalid context descriptor!");
        ERR_print_errors_fp(stderr);
        abort();
    }
    return ctx;
}

void OpenSslClient::loadCertificates(SSL_CTX *ctx, std::string certFile, std::string keyFile) {
    if (SSL_CTX_use_certificate_file(ctx, certFile.data(), SSL_FILETYPE_PEM) <= 0) {
        logger::error("OpenSSL", "Invalid certificate!");
        ERR_print_errors_fp(stderr);
        abort();
    }
    /* set the private key from KeyFile (may be the same as CertFile) */
    if (SSL_CTX_use_PrivateKey_file(ctx, keyFile.data(), SSL_FILETYPE_PEM) <= 0) {
        logger::error("OpenSSL", "Invalid private key!");
        ERR_print_errors_fp(stderr);
        abort();
    }
    /* verify private key */
    if (!SSL_CTX_check_private_key(ctx)) {
        logger::error("OpenSSL", "Private key does not match the public certificate!");
        abort();
    }
}

void OpenSslClient::onConnection(SSL *ssl, ProtocolListener *protocolListener) {
    char buf[1024];
    int sd, bytes;

    if (SSL_accept(ssl) == -1) {
        logger::error("OpenSSL", "Failed to accept the SSL certificate !");
    } else {
        // optimistic parse of the message
        bytes = SSL_read(ssl, buf, sizeof(buf));
        if (bytes > 0) {
            buf[bytes] = 0;
            Message *message = Message::create(buf, (unsigned long) bytes);
            if (message->getType() == protocol) {
                processProtocol((ProtocolMessage *) message, ssl);
            } else {
                protocolListener->messageReceived(*message);
                Message *result = new ProtocolMessage(ack);
                sslWrite(result, ssl);
                delete result;
            }
            delete message;
        }
    }
    sd = SSL_get_fd(ssl);
    SSL_free(ssl);
    ::close(sd);
}

void OpenSslClient::publish(Message &message) {
    publishDelegate(message);
}

bool OpenSslClient::publishDelegate(Message &message) {
    if (message.getSourceId() == 0) {
        message.setSourceId(clientId);
    }

    ProtocolMessage reply = SimpleOpenSslClient::sendSslMessage(remoteHost, remotePort, &message);
    if (reply.getProtocolCommand() != ack) {
        logger::info("OpenSSL", "Host not able to receive the message ...");
        return false;
    } else {
        logger::debug("OpenSSL", "Message successfully sent@" + remoteHost + ":" + to_string(remotePort) + " -> " +
                                 message.toString());
        return true;
    }
}

bool OpenSslClient::joinProtocol() {
    ProtocolMessage msg(join, getLocalAddress(), localPort);
    return publishDelegate(msg);
}

bool OpenSslClient::leaveProtocol() {
    ProtocolMessage msg(leave);
    return publishDelegate(msg);
}

void OpenSslClient::sslWrite(Message *message, SSL *ssl) {
    string serialisation = message->serialise();
    SSL_write(ssl, serialisation.data(), (int) serialisation.size());
}

void OpenSslClient::processProtocol(ProtocolMessage *message, SSL *ssl) {
    // there are no protocol messages to handle on client side, for now
    logger::debug("OpenSSL", "Received protocol message: " + message->toString());
}


std::string OpenSslClient::getLocalAddress() {
    struct ifaddrs *ifAddrStruct = NULL;
    struct ifaddrs *ifa = NULL;
    void *tmpAddrPtr = NULL;

    getifaddrs(&ifAddrStruct);

    string loAddress = "localhost";
    string result = "";

    for (ifa = ifAddrStruct; ifa != NULL; ifa = ifa->ifa_next) {
        if (ifa->ifa_addr) {
            if (ifa->ifa_addr->sa_family == AF_INET) {
                // is a valid IP4 Address
                tmpAddrPtr = &((struct sockaddr_in *) ifa->ifa_addr)->sin_addr;
                char addressBuffer[INET_ADDRSTRLEN];
                inet_ntop(AF_INET, tmpAddrPtr, addressBuffer, INET_ADDRSTRLEN);
                if (strcmp(ifa->ifa_name, "lo") != 0) {
                    result = string(addressBuffer);
                    break;
                } else {
                    loAddress = string(addressBuffer);
                }
            }
        }
    }
    if (ifAddrStruct != NULL) {
        freeifaddrs(ifAddrStruct);
    }

    if (result.empty()) {
        return loAddress;
    } else {
        return result;
    }
}
