//
// Created by ovidiu on 13/10/15.
//

#include <string.h>
#include "MqttClient.h"
#include "../../helper/Helper.h"

using namespace std;

void MqttClient::publish(Message &message) {
    if (message.getSourceId() == 0) {
        message.setSourceId(clientId);
    }
    string serialization = message.serialise();
    const char *buf = serialization.data();
    mosqpp::mosquittopp::publish(NULL, CH_COMPONENT, (int) strlen(buf), buf);
    logger::info("MQTT", "Message sent: " + serialization);
}

void MqttClient::run(ProtocolListener *protocolListener) {
    this->protocolListener = protocolListener;
    connect(brokerHostname.data(), brokerPort, 60);
    logger::info("MQTT", "Connecting to broker: " + brokerHostname + ":" + to_string(brokerPort));
    while (isRunning()) {
        int rc = loop();
        if (rc) {
            reconnect();
        }
    }
}

void MqttClient::closeDelegate() {
    disconnect();
}

void MqttClient::on_connect(int rc) {
    if (rc == 0) {
        logger::debug("MQTT", "Library connected ...");
        subscribe(NULL, CH_CORE_APP, 0);
    }
}

void MqttClient::on_message(const struct mosquitto_message *message) {
    if (protocolListener != NULL) {
        Message *m = Message::create(message->payload, (unsigned long) message->payloadlen);
        if (m->getSourceId() != clientId) {
            protocolListener->messageReceived(*m);
        }
        delete m;
    }
}

void MqttClient::on_log(int level, const char *str) {
    logger::debug("MQTT", "Library Log message:" + string(str));
}

void MqttClient::on_error() {
    logger::debug("MQTT", "Library error!");
}
