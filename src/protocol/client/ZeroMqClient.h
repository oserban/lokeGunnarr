//
// Created by ovidiu on 13/10/15.
//

#ifndef LOKEGUNNARR_ZEROMQCLIENT_H
#define LOKEGUNNARR_ZEROMQCLIENT_H


#include <string>
#include "../../../lib/zmq/zmq.hpp"
#include "../../message/Message.h"
#include "GenericClient.h"

/*
 * The ZeroMQ implementation of the client.
 * Since the client only connects to the server, the hostname is needed for both the publishing and subscribing sockets.
 *
 * The client and the module uses a publish/subscribe communication paradigm
 */

class ZeroMqClient : public GenericClient {
private:
    zmq::context_t *context;
    zmq::socket_t *subscribeSocket;
    zmq::socket_t *publishSocket;

    std::string protocol;
    std::string hostname;
    int publishPort;
    int subscribePort;

public:
    ZeroMqClient(unsigned long clientId, std::string protocol, std::string hostname, int subscribePort, int publishPort);

    virtual ~ZeroMqClient();

private:
    void run(ProtocolListener *protocolListener) override;

    void publish(Message &message) override;


protected:
    virtual void closeDelegate() override;
};


#endif //LOKEGUNNARR_ZEROMQCLIENT_H
