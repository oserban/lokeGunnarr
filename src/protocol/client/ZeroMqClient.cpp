//
// Created by ovidiu on 13/10/15.
//

#include "ZeroMqClient.h"
#include "../../helper/Helper.h"

using namespace zmq;
using namespace std;

ZeroMqClient::ZeroMqClient(unsigned long clientId, std::string protocol, std::string hostname, int subscribePort,
                           int publishPort)
        : GenericClient(clientId) {
    context = new context_t(1);
    subscribeSocket = new socket_t(*context, ZMQ_SUB);
    publishSocket = new socket_t(*context, ZMQ_PUB);

    this->protocol = protocol;
    this->hostname = hostname;
    this->subscribePort = subscribePort;
    this->publishPort = publishPort;
}

ZeroMqClient::~ZeroMqClient() {
    delete subscribeSocket;
    delete publishSocket;
}

string getBindAddress(string protocol, string hostname, int port) {
    stringstream ss;
    ss << protocol << "://" << hostname << ":" << port;
    return ss.str();
}

void ZeroMqClient::run(ProtocolListener *protocolListener) {
    if ((subscribeSocket != NULL) && (protocolListener != NULL)) {
        string bindAddress = getBindAddress(protocol, hostname, subscribePort);
        logger::info("ZeroMQ", "Creating subscribe address: " + bindAddress);
        subscribeSocket->connect(bindAddress);
        subscribeSocket->setsockopt(ZMQ_SUBSCRIBE, "", 0);

        bindAddress = getBindAddress(protocol, hostname, publishPort);
        logger::info("ZeroMQ", "Creating publish address: " + bindAddress);
        publishSocket->connect(bindAddress);

        while (isRunning()) {
            zmq::message_t request;

            //  Wait for next request from client
            subscribeSocket->recv(&request);
            if ((subscribeSocket != NULL) && (subscribeSocket->connected())) {
                Message *message = Message::create(request.data(), request.size());
                if (message->getSourceId() != clientId) {
                    protocolListener->messageReceived(*message);
                }
                delete message;
            }
        }
    }
}

void ZeroMqClient::publish(Message &message) {
    if (message.getSourceId() == 0) {
        message.setSourceId(clientId);
    }
    string representation = message.serialise();
    logger::info("ZeroMQ", "Message sent: " + representation);
    publishSocket->send(representation.data(), representation.size(), 0);
}

void ZeroMqClient::closeDelegate() { }
