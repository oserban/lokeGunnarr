//
// Created by ovidiu on 14/10/15.
//

#include <openssl/ssl.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include "SimpleOpenSslClient.h"
#include "../../helper/Helper.h"

int openConnection(std::string hostname, int port) {
    int sd;
    struct hostent *host;
    struct sockaddr_in addr;

    if ((host = gethostbyname(hostname.data())) == NULL) {
        logger::error("SimpleOpenSslClient", "Could not find hostname");
        return -1;
    }
    sd = socket(PF_INET, SOCK_STREAM, 0);
    bzero(&addr, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = *(long *) (host->h_addr);
    if (connect(sd, (sockaddr *) &addr, sizeof(addr)) != 0) {
        close(sd);
        logger::error("SimpleOpenSslClient", "Could not connect to hostname");
        return -1;
    }
    return sd;
}

SSL_CTX *initCTX(void) {
    const SSL_METHOD *method;
    SSL_CTX *ctx;

    OpenSSL_add_all_algorithms();
    SSL_load_error_strings();
    method = SSLv3_client_method();
    ctx = SSL_CTX_new(method);
    if (ctx == NULL) {
        logger::error("SimpleOpenSslClient", "Invalid SSL context!");
        return NULL;
    }
    return ctx;
}

ProtocolMessage SimpleOpenSslClient::sendSslMessage(std::string hostname, int port, Message *message) {
    SSL *ssl;
    char buf[1024];
    int bytes;

    bool success = false;

    SSL_CTX *ctx = initCTX();
    if (ctx != NULL) {
        int server = openConnection(hostname, port);
        if (server >= 0) {
            ssl = SSL_new(ctx);
            SSL_set_fd(ssl, server);
            if (SSL_connect(ssl) == -1) {
                logger::error("SimpleOpenSslClient", "Could not perform SSL connect");
                success = false;
            } else {
                std::string serialise = message->serialise();
                SSL_write(ssl, serialise.data(), (int) serialise.size());
                bytes = SSL_read(ssl, buf, sizeof(buf));
                if (bytes > 0) {
                    success = true;
                }
                SSL_free(ssl);
            }
            close(server);
        }
        SSL_CTX_free(ctx);
    }
    if (success) {
        return ProtocolMessage(ack);
    } else {
        return ProtocolMessage(reject);
    }
}
