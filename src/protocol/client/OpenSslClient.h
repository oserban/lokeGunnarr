//
// Created by ovidiu on 14/10/15.
//

#ifndef LOKEGUNNARR_OPENSSLCLIENT_H
#define LOKEGUNNARR_OPENSSLCLIENT_H


#include <openssl/ssl.h>
#include "GenericClient.h"
#include "../../message/ProtocolMessage.h"
#include <string>

/*
 * The OpenSSL implementation of a client
 * --
 * Processes simple messages and can send simple or protocol messages (join or leave)
 */

class OpenSslClient : public GenericClient {
private:
    int localPort;
    SSL_CTX *ctx;
    int serverHandler;
    std::string remoteHost;
    int remotePort;
public:
    OpenSslClient(unsigned long clientId, int localPort, std::string certFile, std::string keyFile,
                  std::string remoteHost, int remotePort);

    virtual ~OpenSslClient();

    virtual void publish(Message &message) override;

    bool joinProtocol();

    bool leaveProtocol();

protected:
    virtual void run(ProtocolListener *protocolListener) override;

    virtual void closeDelegate() override;

private:
    int openServerListener(int port);

    SSL_CTX *initServerCTX();

    void loadCertificates(SSL_CTX *ctx, std::string certFile, std::string keyFile);

    void onConnection(SSL *ssl, ProtocolListener *protocolListener);

    // helper methods
    void processProtocol(ProtocolMessage *message, SSL *ssl);

    void sslWrite(Message *message, SSL *ssl);

    bool publishDelegate(Message &message);

    std::string getLocalAddress();
};


#endif //LOKEGUNNARR_OPENSSLCLIENT_H
