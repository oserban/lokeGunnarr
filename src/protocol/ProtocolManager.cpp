//
// Created by ovidiu on 12/10/15.
//

#include <thread>
#include "ProtocolManager.h"
#include "../helper/Helper.h"
#include "module/ZeroMqModule.h"
#include "module/MqttModule.h"
#include "module/OpenSslModule.h"

using namespace std;

void ProtocolManager::addZeroMqModule(std::string protocol, int subscribePort, int publishPort) {
    ZeroMqModule *module = new ZeroMqModule(protocol, subscribePort, publishPort);
    modules.push_back(module);
}

void ProtocolManager::addMqttModule(std::string brokerHostname, int brokerPort) {
    MqttModule *module = new MqttModule(managerId, brokerHostname, brokerPort);
    modules.push_back(module);
}


void ProtocolManager::addOpenSslModule(int listeningPort, std::string certFile, std::string keyFile) {
    OpenSslModule *module = new OpenSslModule(listeningPort, certFile, keyFile);
    modules.push_back(module);
}

void ProtocolManager::removeModule(ModuleType module) {
    for (std::list<GenericModule *>::const_iterator it = modules.begin(), end = modules.end(); it != end; ++it) {
        if (module == (*it)->getType()) {
            modules.remove(*it);
            delete *it;
        }
    }
}


void ProtocolManager::messageReceived(Message &message) {
    logger::info("ProtocolManager", "Message received: " + message.toString());
    if (processingStrategy != NULL) {
        processingStrategy->process(message, this);
    }
}


void ProtocolManager::publish(Message &message) {
    if (message.getSourceId() == 0) {
        message.setSourceId(managerId);
    }

    for (list<GenericModule *>::const_iterator it = modules.begin(), end = modules.end(); it != end; ++it) {
        (*it)->publish(message);
        logger::info("ProtocolManager", "Message published@" + getModuleName((*it)->getType()) + ": " + message.toString());
    }
}

void ProtocolManager::start() {
    for (list<GenericModule *>::const_iterator it = modules.begin(), end = modules.end(); it != end; ++it) {
        (*it)->start(this);
    }
}

void ProtocolManager::close() {
    for (list<GenericModule *>::const_iterator it = modules.begin(), end = modules.end(); it != end; ++it) {
        (*it)->close();
    }

    for (list<GenericModule *>::const_iterator it = modules.begin(), end = modules.end(); it != end; ++it) {
        delete *it;
    }
}
