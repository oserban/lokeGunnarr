//
// Created by ovidiu on 13/10/15.
//

#ifndef LOKEGUNNARR_PUBLISHER_H
#define LOKEGUNNARR_PUBLISHER_H


#include "../message/Message.h"

/*
 * A publisher describes the publishing capability of a class
 */

class Publisher {
public:
    virtual void publish(Message& message) = 0;
};


#endif //LOKEGUNNARR_PUBLISHER_H
