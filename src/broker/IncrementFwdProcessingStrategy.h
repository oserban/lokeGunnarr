//
// Created by ovidiu on 13/10/15.
//

#ifndef LOKEGUNNARR_INCREMENTFWDPROCESSINGSTRATEGY_H
#define LOKEGUNNARR_INCREMENTFWDPROCESSINGSTRATEGY_H


#include "../protocol/GenericProcessingStrategy.h"

/*
 * A Core App can have multiple processing strategies for the received messages.
 * This strategy increments the numeric part of a message and forwards the result.
 */

class IncrementFwdProcessingStrategy: public GenericProcessingStrategy {

public:
    virtual void process(Message &message, Publisher *publisher);
};


#endif //LOKEGUNNARR_INCREMENTFWDPROCESSINGSTRATEGY_H
