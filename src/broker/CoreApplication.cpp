//
// Created by ovidiu on 12/10/15.
//

#include <iostream>
#include <rude/config.h>
#include "../protocol/ProtocolManager.h"
#include "IncrementFwdProcessingStrategy.h"
#include "../helper/Helper.h"
#include "../message/SimpleMessage.h"


#define SECTION_CORE "coreApp"
#define PROPERTY_MANAGER_ID "manager.id"
#define PROPERTY_MODULE_ZMQ_PROTOCOL "module.zmq.protocol"
#define PROPERTY_MODULE_ZMQ_SUBSCRIBE "module.zmq.subscribe.port"
#define PROPERTY_MODULE_ZMQ_PUBLISH "module.zmq.publish.port"
#define PROPERTY_MODULE_MQTT_IP "module.mqtt.broker.ip"
#define PROPERTY_MODULE_MQTT_PORT "module.mqtt.broker.port"
#define PROPERTY_MODULE_OPENSSL_PORT "module.openssl.port"
#define PROPERTY_MODULE_OPENSSL_CERTIFICATE "module.openssl.certificate"
#define PROPERTY_MODULE_OPENSSL_KEY "module.openssl.key"

/*
 * The CORE and main application, which acts as a bridge (broker) between multiple protocols
 */

using namespace std;
using namespace rude;

int main(int args, char **argv) {
    if (args != 2) {
        logger::error("CoreApplication", "Usage: " + string(argv[0]) + " <project conf file>");
        return 1;
    }

    Config config;
    if (config.load(argv[1])) {
        config.setSection(SECTION_CORE);
        // Increment and Forward message strategy
        IncrementFwdProcessingStrategy *strategy = new IncrementFwdProcessingStrategy();
        ProtocolManager manager((unsigned long) config.getIntValue(PROPERTY_MANAGER_ID), strategy);
        // load ZeroMQ module
        manager.addZeroMqModule(config.getStringValue(PROPERTY_MODULE_ZMQ_PROTOCOL),
                                config.getIntValue(PROPERTY_MODULE_ZMQ_SUBSCRIBE),
                                config.getIntValue(PROPERTY_MODULE_ZMQ_PUBLISH));
        // load Mosquito module
        manager.addMqttModule(config.getStringValue(PROPERTY_MODULE_MQTT_IP),
                              config.getIntValue(PROPERTY_MODULE_MQTT_PORT));

        string serverSslCertificate = config.getStringValue(PROPERTY_MODULE_OPENSSL_CERTIFICATE);
        if (!file::fileExists(serverSslCertificate)) {
            logger::error("CoreApplication", "Could not find certificate file: " + serverSslCertificate);
            return 1;
        }
        string serverSslKey = config.getStringValue(PROPERTY_MODULE_OPENSSL_KEY);
        if (!file::fileExists(serverSslKey)) {
            logger::error("CoreApplication", "Could not find ssl key file: " + serverSslKey);
            return 1;
        }
        // load OpenSSL module
        manager.addOpenSslModule(config.getIntValue(PROPERTY_MODULE_OPENSSL_PORT), serverSslCertificate, serverSslKey);
        manager.start();

        string line = " ";
        while (!line.empty()) {
            logger::info("CoreApplication", "Enter your message (an empty line will end the program): ");
            getline(cin, line);
            if (!line.empty()) {
                // the message is a concatenation of a random int + the line read from stdin
                Message *message = new SimpleMessage(rand(), line);
                manager.publish(*message);
                delete message;
            }
        }
        manager.close();
        return 0;
    } else {
        logger::error("CoreApplication", "Could not load configuration file: " + string(argv[1]));
        return 1;
    }
}