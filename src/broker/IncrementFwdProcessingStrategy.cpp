//
// Created by ovidiu on 13/10/15.
//

#include "IncrementFwdProcessingStrategy.h"
#include "../message/SimpleMessage.h"

void IncrementFwdProcessingStrategy::process(Message &message, Publisher *publisher) {
    SimpleMessage *copyMessage = (SimpleMessage *) message.clone();
    copyMessage->setNumber(copyMessage->getNumber() + 1);
    publisher->publish(*copyMessage);
    delete copyMessage;
}
