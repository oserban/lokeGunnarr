cmake_minimum_required(VERSION 3.0)
project(lokeGunnarr)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set (CMAKE_INCLUDE_PATH ${CMAKE_INCLUDE_PATH} /usr/local/include)
foreach(path ${CMAKE_INCLUDE_PATH})
    include_directories(${path})
endforeach()
set (CMAKE_LIBRARY_PATH ${CMAKE_LIBRARY_PATH} /usr/local/lib)
foreach(path ${CMAKE_LIBRARY_PATH})
    link_directories(${path})
endforeach()

# Find required libraries
find_package(Threads REQUIRED)

# Broker build
set(APP_NAME_BROKER lokeGunnarr-broker)

set(SOURCE_FILES_BROKER
        lib/zmq/zmq.hpp
        src/broker/CoreApplication.cpp
        src/broker/IncrementFwdProcessingStrategy.cpp src/broker/IncrementFwdProcessingStrategy.h
        src/message/Message.h src/message/Message.cpp
        src/message/SimpleMessage.cpp src/message/SimpleMessage.h
        src/message/ProtocolMessage.cpp src/message/ProtocolMessage.h
        src/message/ProtocolCommand.cpp src/message/ProtocolCommand.h
        src/message/MessageType.h
        src/helper/Helper.h src/helper/Helper.cpp
        src/protocol/ProtocolListener.h
        src/protocol/ProtocolManager.cpp src/protocol/ProtocolManager.h
        src/protocol/module/GenericModule.h
        src/protocol/module/ZeroMqModule.cpp src/protocol/module/ZeroMqModule.h
        src/protocol/module/ModuleType.h src/protocol/module/ModuleType.cpp
        src/protocol/GenericProcessingStrategy.h
        src/protocol/Publisher.h
        src/protocol/module/MqttModule.cpp src/protocol/module/MqttModule.h
        src/protocol/module/OpenSslModule.cpp src/protocol/module/OpenSslModule.h
        src/protocol/client/SimpleOpenSslClient.cpp src/protocol/client/SimpleOpenSslClient.h)

add_executable(${APP_NAME_BROKER} ${SOURCE_FILES_BROKER})
target_link_libraries(${APP_NAME_BROKER} rudeconfig pthread zmq mosquitto mosquittopp crypto ssl)

# 0MQ App build
set(APP_NAME_ZMQ lokeGunnarr-zmq)

set(SOURCE_FILES_ZMQ_APP
        lib/zmq/zmq.hpp
        src/app/ZeroMqApp.cpp
        src/app/ComponentApp.cpp src/app/ComponentApp.h
        src/message/Message.h src/message/Message.cpp
        src/message/SimpleMessage.cpp src/message/SimpleMessage.h
        src/message/ProtocolMessage.cpp src/message/ProtocolMessage.h
        src/message/ProtocolCommand.cpp src/message/ProtocolCommand.h
        src/message/MessageType.h
        src/helper/Helper.h src/helper/Helper.cpp
        src/protocol/ProtocolListener.h
        src/protocol/client/GenericClient.h
        src/protocol/client/ZeroMqClient.cpp src/protocol/client/ZeroMqClient.h
)

add_executable(${APP_NAME_ZMQ} ${SOURCE_FILES_ZMQ_APP})
target_link_libraries(${APP_NAME_ZMQ} zmq pthread rudeconfig)

# MQTT App build
set(APP_NAME_MQTT lokeGunnarr-mqtt)

set(SOURCE_FILES_MQTT_APP
        src/app/MqttApp.cpp
        src/app/ComponentApp.cpp src/app/ComponentApp.h
        src/message/Message.h src/message/Message.cpp
        src/message/SimpleMessage.cpp src/message/SimpleMessage.h
        src/message/ProtocolMessage.cpp src/message/ProtocolMessage.h
        src/message/ProtocolCommand.cpp src/message/ProtocolCommand.h
        src/message/MessageType.h
        src/helper/Helper.h src/helper/Helper.cpp
        src/protocol/ProtocolListener.h
        src/protocol/client/GenericClient.h
        src/protocol/client/MqttClient.h src/protocol/client/MqttClient.cpp)

add_executable(${APP_NAME_MQTT} ${SOURCE_FILES_MQTT_APP})
target_link_libraries(${APP_NAME_MQTT} pthread rudeconfig mosquitto mosquittopp)

# OpenSSL App build
set(APP_NAME_OPENSSL lokeGunnarr-openssl)

set(SOURCE_FILES_OPENSSL_APP
        src/app/OpenSslApp.cpp
        src/app/ComponentApp.cpp src/app/ComponentApp.h
        src/message/Message.h src/message/Message.cpp
        src/message/SimpleMessage.cpp src/message/SimpleMessage.h
        src/message/ProtocolMessage.cpp src/message/ProtocolMessage.h
        src/message/ProtocolCommand.cpp src/message/ProtocolCommand.h
        src/message/MessageType.h
        src/helper/Helper.h src/helper/Helper.cpp
        src/protocol/ProtocolListener.h
        src/protocol/client/GenericClient.h
        src/protocol/client/OpenSslClient.h src/protocol/client/OpenSslClient.cpp
        src/protocol/client/SimpleOpenSslClient.h src/protocol/client/SimpleOpenSslClient.cpp)

add_executable(${APP_NAME_OPENSSL} ${SOURCE_FILES_OPENSSL_APP})
target_link_libraries(${APP_NAME_OPENSSL} pthread rudeconfig crypto ssl)