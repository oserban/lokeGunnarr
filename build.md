# Short build tutorial

## Prerequisites:

1. Build tools: make, cmake, c++ compiler (with at least C++ 11 supported)
2. rude::config library (version 5.0.5): http://rudeserver.com/config/download.html
	Download, extract the archive, make && make install
3. ZeroMQ library (version 4.1.3): http://zeromq.org/intro:get-the-software
	Download, extract the archive, make && make install
4. Mosquito library (version 1.4.4): http://mosquitto.org/download/
	Download, extract the archive, make && make install
5. OpenSSL development library (version 1.0.1, Ubuntu 1.0.1f-1ubuntu11.4): apt-get install libssl-dev or https://www.openssl.org/source/

**! Please note:** On Ubuntu and Debian, before compiling the project, the ld registry has to be updated:

```bash
sudo ldconfig
```

**! On Mac OS:** Most of these tools and libraries can be installed with brew
	 

## Building the project:

```bash
cd build\ && cmake .. && make
```