\documentclass[12pt]{article}
\usepackage{url}
\usepackage{listings}
\usepackage{xcolor}
\usepackage{combelow}
\usepackage{amssymb}
\usepackage[a4paper, margin=2.5cm]{geometry}



\lstset{basicstyle=\ttfamily,
  showstringspaces=false,
  commentstyle=\color{gray},
  keywordstyle=\color{blue}
}

\title{\textbf{Loke Gunnarr}\footnote{The name of the project has been randomly generated using an online tool}  Project}
\author{Ovidiu \cb Serban, \url{ovidiu@roboslang.org}}
\date{}

\begin{document}
\maketitle

\section{General design}

The goal of this project is to provide a simple integration of various communication protocol and libraries into a single project. Currently, three libraries and protocols were chosen for this purpose: ZeroMQ (\varnothing MQ), Mosquito (MQTT) and OpenSSL sockets (self-signed certificates).

The project is split into two sides: 
\begin{itemize}
	\item \textbf{Modules}: containing all the communication protocols and integration with one library, used by a \textit{Protocol Manager}, integrated into the \textit{Core Application}.
	\item \textbf{Clients}: are an implementation of a single communication protocol (integration with a library) and can be used by the Apps to communicate with the modules. 
\end{itemize}

\paragraph{Modules:} All the modules extend from a \textit{Generic Module} class, which is in charge of all the internal management of the module, including running the message listener code on a separate thread. Once a new message is received, the module will notify the \textit{Protocol Manager} on all the accepted messages. The \textit{Protocol Manager} will apply a \textbf{Processing Strategy} (a subclass of the \textit{Generic Processing Strategy}) on the received messages. This strategy is in charge of any processing happening on the \textit{Core Application} side and can be easily created or extended, without modifying the modules.

\paragraph{Clients:} On the other side, all the clients extend a \textit{Generic Client} class, which, similar to the modules, implement a single protocol/library. The \textit{Generic Client} is also in charge of running all the code on a separate thread, to help the  client receive all the responses. Once a new message is received, the client will notify a listener. One example of such a listener is the \textit{ComponentApp} which prints out all the received messages. 

\paragraph{Unique component IDs:} In order to  make the make the message filtering strategies easier (described later), each message will provide a source id. This unique id needs to be provided for each client (therefore configured for each individual App) and once for the \textit{Core Application}. The unique Id of the \textit{Core Application} will be shared between all the modules.

\subsection{Data and message design}

While all the included libraries tend to be data agnostic, in my view data-oriented applications can be better optimised from the traffic point of view and incoming and outgoing messages can be validated against the component's contract (preconditions and postconditions). 
Currently, the project uses two types of data: protocol (which handles some protocol commands for OpenSSL implementation) and simple data (which contains a number and some text information).

All the data transferred by both the clients and modules need to be formalised as messages, and become subclasses of the \textit{Message} class. The \textit{Message} has a very low memory footprint (contains only the source id) and contains useful operations for serialisation and de-serialisations (using specialised constructors), plus some other helper methods. Currently, the serialisation operations are able to handle a concatenation of all the data fields, some fields are serialised by the \textit{Message} class (such as \textit{source Id} and \textit{message type}), while the rest are delegated to the subclasses. 

\subsection{Message filtering strategies}

One of the requirements is to broadcast any message received by the  \textit{ComponentApp} to all the connected Apps, except the originator of the message. This filtering is done by using the unique source id of the message.

Because of some restrictions of the libraries used, two filtering strategies have been defined:

\begin{itemize}
	\item \textbf{Module side filtering}: For modules that allow direct client connections, such a OpenSSL, filtering at module level is a very easy and clean solution.
	\item \textbf{Client side filtering}: For publish-subscribe modules, such as ZeroMQ and MQTT, filtering at the module level is more complicated. For this reason, the message is forwarded back to the client which simply ignores it. From the privacy and security point of view, this can be done safely, because the client will ignore its own message.
\end{itemize}

\section{Modules}
\subsection{Mosquito}

The Mosquito library provides a base class to extend for direct broker connections.    Since Mosquito uses a publish-subscribe message queue protocol only, two separate broadcasting queues (identified by topics) were created to avoid loops: 

\begin{lstlisting}[language=c++]
// the component channel is used by any client to send 
// messages to the Core App
#define CH_COMPONENT "mqtt/component"
// the core App channel is used to broadcast all the messages 
// from the Core App
#define CH_CORE_APP "mqtt/coreApp"
\end{lstlisting}

\subsection{ZeroMQ}

ZeroMQ is able to handle multiple communication paradigms, such as request-reply and publish-subscribe. For the purpose of this project, a publish-subscribe communication was selected, similar to the MQTT implementation, with a separate publish and subscribe connection. Moreover, the library is able to bind and connect asynchronously, and for this application, the assumption was that the module will be binding for both the publish and subscribe connections, while the clients will only connect. For publish-subscribe paradigms, ZeroMQ provides support to define topics, but they were not used for this implementation (modules and clients are listening to everything exchanged between them).

\subsection{OpenSSL}

The OpenSSL, apart from encrypted transport, does not provide more advanced socket management tools, such as the other two libraries. In order to achieve a similar infrastructure and integration for all three libraries, a simple protocol has been designed for OpenSSL. 

The module binds a secure server socket connection, by using a certificate and a secure key (which can be generated using the OpenSSL tools). The client can connect to the sever using a secured socket and upon connection the TLS handshake will take place. This is done automatically by the OpenSSL library and ensures an inbound connection between the client and the module. 

For the outbound connection, there are multiple solutions to consider and considering that in the world of IoT, low powered non-stable connections are more frequent, a fully disconnected solution was preferred. The client will start a secure server socket as well and will allow the module to connect every time it is required. To ensure that the connections are not kept longer than required, the socket connections are disconnected immediately after the socket transfer. 

The only issue with the outbound connection is that the module needs to know the address and port of the client server. This is solved with a simple protocol:
\begin{itemize}
	\item After its first connection, an OpenSSL client is required to notify the module about its hostname and listening port with a \textit{join} command. \\ Command: \verb!join hostname:port!. If the connection is accepted, the module will reply with an \textit{ack} or a \textit{reject}, otherwise.
	\item Before quitting the current session, the client needs to notify the module with a \textit{leave} command. To increase the robustness of the protocol, if a broadcast to an OpenSSL client is failed, the module will trigger the \textit{leave} protocol immediately. 
	\item If a client does not respect the protocol and sends a message before joining, the module will reject the message by notifying the client as well. 
\end{itemize}

\section{Current limitations and possible extensions}

The current implementation stands as a proof of concept that all the libraries can be integrated and has a few limitations, with some possible extensions, depending on the requirements of the extended project.

\paragraph{Unique Id validation:} The current system adopts a naive approach towards the unique id validation strategy and lets the developer choose and validate the ids manually. Potentially, these could be validated automatically, using a public key encryption protocol or something similar. However, an automatic validation protocol could prove more computationally expensive for low powered devices and my current solution may be more practical.

\paragraph{Secure transport:} Apart from the OpenSSL sockets used within the project, the other transport layers are unsecured. Both ZeroMQ and MQTT offer secure implementations, but such implementation tend not to work for popular low powered devices, such as Arduino boards. Depending on the target, secure transport may be considered for other modules.

\paragraph{OpenSSL module limitations:} The current implementation of the OpenSSL module and client makes the strong assumption that the client ip and port is reachable. While this can be ensured via secured tunnels or VPNs, there are certain situations where this assumption is no longer valid. To overcome this issue, a non real-time can be used (similar to MQTT), by adding a message queue on the module side, to store all the messages to be published on a socket, once the client reconnects. Similar to MQTT, the client can establish a will of how many messages to be store and for how long.

\section{Building the project}

The project can be cloned from Gitlab:

\begin{lstlisting}[language=bash]
git clone https://gitlab.com/oserban/lokeGunnarr.git
\end{lstlisting}

\subsection{Prerequisites}

\begin{itemize}
	\item Several build tools are required: make, cmake, C++ compiler (C++ 11 supported)
	\item rude::config library (version 5.0.5): \url{http://rudeserver.com/config/download.html} \\
	Download, extract the archive, \verb!make && make install!
\item ZeroMQ library (version 4.1.3): \url{http://zeromq.org/intro:get-the-software}\\
	Download, extract the archive, \verb!make && make install!
\item Mosquito library (version 1.4.4): \url{http://mosquitto.org/download/}\\
	Download, extract the archive, \verb!make && make install!
\item OpenSSL development library (version 1.0.1): \verb!apt-get install libssl-dev! or \url{https://www.openssl.org/source/}
\end{itemize}

\textbf{Note:} On Ubuntu and Debian, before compiling the project, the ld registry has to be updated: \verb!sudo ldconfig!

\textbf{Note:} On Mac OS, most of these tools and libraries can be installed with \verb!brew!

\subsection{Build}
From the project's main folder:

\begin{lstlisting}[language=bash]
cd build\ && cmake .. && make
\end{lstlisting}

\section{Testing}

The main configuration file, called \verb!project.conf! (located under the r\verb!es\! folder), contains one profile (section) dedicated to the broker and 3 profiles used to configure the three applications (ZeroMQ, MQTT or OpenSSL). The OpenSSL profile has a second configuration to test more than one connection to the CoreApp. To start, change the folder to \verb!res\! and execute the following commands:

\begin{itemize}
	\item Loke Gunnarr Broker: 
	\begin{lstlisting}[language=bash]
../build/lokeGunnarr-broker project.conf
	\end{lstlisting}
	\item Loke Gunnarr ZeroMQ App: 
	\begin{lstlisting}[language=bash]
../build/lokeGunnarr-zmq project.conf ZeroMQApp
	\end{lstlisting}
	\item Loke Gunnarr MQTT App: 
	\begin{lstlisting}[language=bash]
../build/lokeGunnarr-mqtt project.conf MQTTApp
	\end{lstlisting}
	\item Loke Gunnarr OpenSSL App: 
	\begin{lstlisting}[language=bash]
../build/lokeGunnarr-openssl project.conf OpenSSLApp
	\end{lstlisting}
	\item Loke Gunnarr OpenSSL App (2): 
	\begin{lstlisting}[language=bash]
../build/lokeGunnarr-openssl project.conf OpenSSLApp2
	\end{lstlisting}
\end{itemize}

\textbf{Note:} Each application will end if an empty message is entered.

\end{document}